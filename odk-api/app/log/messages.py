"""
Definitions for common exceptions and errors,
to be used across the application
"""

JSON_DECODE_ERROR = "[JSONDecodeError] Message not in JSON format"
KEY_ERROR = "[KeyError] Following key not found: {}"
